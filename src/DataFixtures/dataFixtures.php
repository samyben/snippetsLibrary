<?php

namespace App\DataFixtures;

use App\Entity\Languages;
use App\Entity\Users;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class dataFixtures extends Fixture
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
//        $user1 = new Users('Samy');
//        $user1->setEmail('samyben2001@hotmail.com');
//        $manager->persist($user1);
//        $user2 = new Users('Jeremy');
//        $user2->setEmail('jeremy@misterjekyll.be');
//        $manager->persist($user2);
//        $user3 = new Users('Charles');
//        $user3->setEmail('charles@misterjekyll.be');
//        $manager->persist($user3);
//        $user4= new Users('Clement');
//        $user4->setEmail('clement@misterjekyll.be');
//        $manager->persist($user4);
//        $manager->flush();

        //ajout langages en DB
        $lang1 = new Languages('HTML');
        $manager->persist($lang1);
        $lang2 = new Languages('CSS');
        $manager->persist($lang2);
        $lang3 = new Languages('PHP');
        $manager->persist($lang3);
        $lang4 = new Languages('JS');
        $manager->persist($lang4);
        $lang5 = new Languages('SQL');
        $manager->persist($lang5);
        $lang6 = new Languages('JSON');
        $manager->persist($lang6);
        $manager->flush();
    }
}