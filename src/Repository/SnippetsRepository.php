<?php

namespace App\Repository;

use App\Entity\Snippets;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class SnippetsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Snippets::class);
    }

    public function getAll()
    {
        return $this->createQueryBuilder('s')
            ->select('s')
            ->join('s.files', 'f')
            ->join('s.user', 'u')
            ->join('f.language', 'l')
            ->orderBy('s.title', 'ASC')
            ->getQuery();

    }

    public function getByTitle($title)
    {
        return $this->createQueryBuilder('s')
            ->select('s')
            ->where('s.title LIKE :title')->setParameter('title', addcslashes($title,'%_').'%')
            ->join('s.files', 'f')
            ->join('s.user', 'u')
            ->join('f.language', 'l')
            ->orderBy('s.title', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function getByTitleAndUser($title, $user)
    {
        return $this->createQueryBuilder('s')
            ->select('s')
            ->where('s.title LIKE :title')->setParameter('title', addcslashes($title,'%_').'%')
            ->andWhere('u.id = :user')->setParameter('user', $user)
            ->join('s.files', 'f')
            ->join('s.user', 'u')
            ->join('f.language', 'l')
            ->orderBy('s.title', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function getOne($id)
    {
        return $this->createQueryBuilder('s')
            ->select('s')
            ->where('s.id = :id')->setParameter('id', $id)
            ->join('s.files', 'f')
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findByLanguage($value)
    {
        return $this->createQueryBuilder('s')
            ->select('s')
            ->join('s.files', 'f')
            ->join('f.language', 'l')
            ->where('l.name = :value')->setParameter('value', $value)
            ->orderBy('s.title', 'ASC')
            ->getQuery();
    }

    public function findByUser($user)
    {
        return $this->createQueryBuilder('s')
            ->select('s')
            ->join('s.user', 'u')
            ->where('u.id = :user')->setParameter('user', $user)
            ->join('s.files', 'f')
            ->join('f.language', 'l')
            ->orderBy('s.title', 'ASC')
            ->getQuery();
    }

    public function findByUserAndLang($user, $lang)
    {
        return $this->createQueryBuilder('s')
            ->select('s')
            ->join('s.user', 'u')
            ->where('u.id = :user')->setParameter('user', $user)
            ->join('s.files', 'f')
            ->join('f.language', 'l')
            ->andWhere('l.name = :lang')->setParameter('lang', $lang)
            ->orderBy('s.title', 'ASC')
            ->getQuery();
    }

}
