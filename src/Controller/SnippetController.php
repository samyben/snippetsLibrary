<?php

namespace App\Controller;

use App\Entity\Files;
use App\Entity\Languages;
use App\Entity\LanguagesSnippets;
use App\Entity\Snippets;
use App\Repository\LanguagesRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class SnippetController extends Controller
{

    public function viewSnippets(Request $request)
    {
        if (isset($_POST['search'])) {
            $search = $_POST['search'];

            $encoders = array(new XmlEncoder(), new JsonEncoder());
            $normalizer = new ObjectNormalizer();
            $normalizer->setCircularReferenceHandler(function ($object) {
                return $object->getId();
            });
            $serializer = new Serializer(array($normalizer), $encoders);

            $arrData = $this->getDoctrine()->getRepository(Snippets::class)->getByTitle($search);
            $arrData = $serializer->serialize($arrData, 'json');

            return new JsonResponse($arrData);
        }

        return $this->renderSnippetView('findByLanguage', 'getAll', 'snippets/home.html.twig', $request);
    }

    public function viewMySnippets(Request $request)
    {
        if (isset($_POST['search'])) {
            $search = $_POST['search'];

            $encoders = array(new XmlEncoder(), new JsonEncoder());
            $normalizer = new ObjectNormalizer();
            $normalizer->setCircularReferenceHandler(function ($object) {
                return $object->getId();
            });
            $serializer = new Serializer(array($normalizer), $encoders);

            $arrData = $this->getDoctrine()->getRepository(Snippets::class)->getByTitleAndUser($search,
                $this->getUser()->getId());
            $arrData = $serializer->serialize($arrData, 'json');

            return new JsonResponse($arrData);
        }

        return $this->renderSnippetView('findByUserAndLang', 'findByUser', 'snippets/mySnippets.html.twig', $request);
    }

    public function snippetDetails($id)
    {
        $snippet = $this->getDoctrine()->getRepository(Snippets::class)->getOne($id);

        return $this->render('snippets/details.html.twig', array('snippet' => $snippet));
    }

    public function addSnippet(ObjectManager $manager)
    {
        /*RECUPERATION DE TOUS LES LANGAGES*/
        $langs = $this->getDoctrine()->getRepository(Languages::class)->findAll();

        if (isset($_POST['submit'])) {
            $this->manageSnippet('add', $manager);
            $this->addFlash('addSnippet', 'Your snippet has been successfully added');
            return $this->redirectToRoute('addSnippet');
        }

        return $this->render('snippets/adding.html.twig', array('langs' => $langs));
    }

    public function updateSnippet(Snippets $snippet, ObjectManager $manager)
    {
        /*RECUPERATION DE TOUS LES LANGAGES*/
        $langs = $this->getDoctrine()->getRepository(Languages::class)->findAll();

        if (isset($_POST['submit'])) {
            $this->manageSnippet('update', $manager, $snippet);
            $this->addFlash('updateSnippet', 'Your snippet has been successfully updated');
            return $this->redirectToRoute('mySnippets');
        }

        return $this->render('snippets/update.html.twig', array('langs' => $langs, 'snippet' => $snippet));
    }

    public function deleteSnippet(Snippets $snippet, ObjectManager $manager)
    {
        foreach ($snippet->getFiles() as $file) {
            $manager->remove($file);
        }
        $manager->remove($snippet);
        $manager->flush();

        return $this->redirectToRoute('snippets');
    }

    private function renderSnippetView($function1, $function2, $view, Request $request)
    {
        /*RECUPERATION DE TOUS LES LANGAGES*/
        $langs = $this->getDoctrine()->getRepository(Languages::class)->findAll();

        if ($_POST !== []) {
            /*RECUPERATION SNIPPETS EN FONCTION DU LANGAGE*/
            if ($function1 == 'findByLanguage') {
                $query = $this->getDoctrine()->getRepository(Snippets::class)->$function1($_POST['languages']);
            } else {
                $query = $this->getDoctrine()->getRepository(Snippets::class)->$function1($this->getUser()->getId(),
                    $_POST['languages']);
            }
        } else {
            /*RECUPERATION SNIPPETS*/
            if ($function2 == 'getAll') {
                $query = $this->getDoctrine()->getRepository(Snippets::class)->$function2();
            } else {
                $query = $this->getDoctrine()->getRepository(Snippets::class)->$function2($this->getUser()->getId());
            }
        }
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            10/*limit per page*/
        );

        return $this->render($view, array(
            'pagination' => $pagination,
            'langs'      => $langs,
        ));
    }

    /*Permet soit d'ajouter ou de modifier un snippet. $type = 'add' ou 'update'*/
    private function manageSnippet($type, ObjectManager $manager, Snippets $snippet = null)
    {
        if ($type == 'add') {
            $snippet = new Snippets();
        }
        $snippet->setUser($this->getUser());
        $snippet->setTitle($_POST['title']);
        $snippet->setDate(new \DateTime());

        if ($type == 'update') {
            /*suppression ancien fichiers*/
            foreach ($snippet->getFiles() as $file) {
                $manager->remove($file);
            }
        }


        foreach ($_POST as $key => $value) {
            if (strpos($key, 'fileName') === 0) {
                $x = explode('_', $key);
                $x = $x[1];
                $file = new Files();
                $file->setSnippet($snippet);
                $file->setFileName($_POST['fileName_' . $x]);
                $file->setContent($_POST['fileContent_' . $x]);
                $lang = $this->getDoctrine()->getManager()->getRepository(Languages::class)->findOneBy(['name' => $_POST['fileLang_' . $x]]);
                $manager->persist($lang);
                $file->setLanguage($lang);
                $manager->persist($file);
            }
        }

        $manager->persist($snippet);
        $manager->flush();
    }
}