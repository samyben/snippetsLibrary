/**
 * Created by Sam on 28/12/2017.
 */
/*****  aceEditor  ******/

$(document).ready(function () {
    var editor = $("textarea[id^='snippetArea_']");

    for (i = 0; i < editor.length; i++) {
        editor[i] = ace.edit("snippetArea_" + i);
        editor[i].setTheme("ace/theme/monokai");
        switch ($('#fileLang_' + i).html()) {
            case 'HTML':
                editor[i].getSession().setMode("ace/mode/html");
                break;
            case 'CSS':
                editor[i].getSession().setMode("ace/mode/css");
                break;
            case 'PHP':
                editor[i].getSession().setMode("ace/mode/php");
                break;
            case 'JS':
                editor[i].getSession().setMode("ace/mode/javascript");
                break;
            case 'SQL':
                editor[i].getSession().setMode("ace/mode/sql");
                break;
            case 'JSON':
                editor[i].getSession().setMode("ace/mode/json");
                break;
            default:
                editor[i].getSession().setMode("ace/mode/html");
        }
        editor[i].setAutoScrollEditorIntoView(true);
        editor[i].setOption("maxLines", 25);
        editor[i].setOption("minLines", 25);
        editor[i].getSession().setUseWrapMode(true);
        editor[i].setReadOnly(true);
    }
})

