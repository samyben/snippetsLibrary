$(document).ready(function () {
    $("input[type='password']").removeAttr('required');
    $('#btnChangeInfos').on('click', function (e) {
        e.preventDefault();
        $('#changePwd').removeClass('hidden');
        $('#formInfos input').removeAttr('disabled');
        $('button[type="submit"]').removeClass('hidden');
        $('#btnChangeInfos').addClass('hidden');
    })

    $('#btnUpdatePwd').on('click', function (e) {
        e.preventDefault();
        $('#updatePwd>.rowInfos').removeClass('hidden');
        $('#btnUpdatePwd').addClass('hidden');
    })
})