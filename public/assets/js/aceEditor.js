/**
 * Created by Sam on 28/12/2017.
 */
$(document).ready(function () {
    /*****  aceEditor  ******/
    var options = $("select[name='fileLang_0'] option");
    var editor = $("textarea[id^='snippetArea_']");
    var filesBox = $("#filesBox");
    var fileCompt;

    /*****  Init aceEditor  ******/
    for (var i = 0; i < editor.length; i++) {
        fileCompt = i;
        editor[i] = ace.edit("snippetArea_" + i);
        editor[i].setTheme("ace/theme/monokai");
        switchAceType($('#fileLang_' + i).val(), i)
        editor[i].setAutoScrollEditorIntoView(true);
        editor[i].setOption("maxLines", 25);
        editor[i].setOption("minLines", 25);
        editor[i].getSession().setUseWrapMode(true);
    }

    /*****changement type ace editor*****/
    filesBox.on('change', "select[name^='fileLang_']", function (e) {
        var nb = $(this).attr('name').split('_');
        nb = nb[1];
        switchAceType($(this).val(), nb)
    });

    /*****ajout div ace editor*****/
    filesBox.on('click', "button[id^='btnAddFile']", function (e) {
        fileCompt++;
        $('#filesBox').append(
            '<div id="snippetContent_' + fileCompt + '">' +
            '<div class="col-md-12">' +
            '<div class="pull-left">' +
            '<label for="fileName_' + fileCompt + '">File name:&nbsp;</label>' +
            '<input type="text" id="fileName_' + fileCompt + '" name="fileName_' + fileCompt + '">' +
            '</div>' +
            '<div class="pull-right">' +
            '<label for="fileLang_' + fileCompt + '">File format:&nbsp;</label>' +
            '<select name="fileLang_' + fileCompt + '" id="fileLang_' + fileCompt + '"></select>' +
            '</div>' +
            '<div class="clearfix"></div>' +
            '</div>' +
            '<label for="snippetArea_' + fileCompt + '" class="hidden"></label>' +
            '<label for="fileContent_' + fileCompt + '" class="hidden"></label>' +
            '<textarea id="snippetArea_' + fileCompt + '" rows = "15"></textarea> ' +
            '<textarea class="hidden" name = "fileContent_' + fileCompt + '" id = "fileContent_' + fileCompt + '"></textarea>' +
            '<div>' +
            '<button type="button" class="btn btn-default" id="btnAddFile_' + fileCompt + '">Add file</button>' +
            '<button type="button" class="btn btn-default" id="btnDelFile_' + fileCompt + '">Delete file</button>' +
            '</div>' +
            '</div>'
        );
        /*ajout des langues dans le select*/
        var select = $("#fileLang_" + fileCompt);
        for (i = 0; i < options.length; i++) {
            var opt = options[i].value;
            select.append('<option value="' + opt + '">' + opt + '</option>')
        }

        editor.push(ace.edit("snippetArea_" + fileCompt));
        editor[fileCompt].setTheme("ace/theme/monokai");
        editor[fileCompt].getSession().setMode("ace/mode/javascript");
        editor[fileCompt].setAutoScrollEditorIntoView(true);
        editor[fileCompt].setOption("maxLines", 40);
        editor[fileCompt].setOption("minLines", 20);
        editor[fileCompt].getSession().setUseWrapMode(true);
    });

    /*****suppression div ace editor*****/
    filesBox.on('click', "button[id^='btnDelFile']", function (e) {
        $(this).parent().parent().remove();
    });

    $("#formSnippet").on('submit', function (e) {
        for (var i = 0; i < editor.length; i++) {
            $('#fileContent_' + i).val(editor[i].getValue());
            console.log($('#fileContent_' + i).val())
        }
    })

    function switchAceType(val, indice) {
        switch (val) {
            case 'HTML':
                editor[indice].getSession().setMode("ace/mode/html");
                break;
            case 'CSS':
                editor[indice].getSession().setMode("ace/mode/css");
                break;
            case 'PHP':
                editor[indice].getSession().setMode("ace/mode/php");
                break;
            case 'JS':
                editor[indice].getSession().setMode("ace/mode/javascript");
                break;
            case 'SQL':
                editor[indice].getSession().setMode("ace/mode/sql");
                break;
            case 'JSON':
                editor[indice].getSession().setMode("ace/mode/json");
                break;
            default:
                editor[indice].getSession().setMode("ace/mode/html");
        }
    }
})