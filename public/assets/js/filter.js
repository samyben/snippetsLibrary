/**
 * Created by Sam on 27/12/2017.
 */
$(document).ready(function () {
    var form = $('#formLanguages');
    var snippetLangs = $('.snippetLang');

    $('#languages').change(function () {
            for (var i = 0; i < snippetLangs.length; i++) {
                $(snippetLangs[i]).parent().addClass('hidden');
                var langs = $(snippetLangs[i]).text().replace(/ /g, '').replace(/\s/g, '').replace(/\r?\n|\r/g, '').split(',');
                console.log(langs)
                for (var j = 0; j < langs.length; j++) {
                    if (langs[j] == $('#languages').val() || $('#languages').val() == 'all') {
                        $(snippetLangs[i]).parent().removeClass('hidden');
                    }
                }
            }

    });

    function formatDate(date) {
        var monthNames = [
            "Jan", "Feb", "Mar",
            "Apr", "May", "Jun", "Jul",
            "Aug", "Sep", "Oct",
            "Nov", "Dec"
        ];

        var day = date.getDate();
        var monthIndex = date.getMonth();
        var year = date.getFullYear();

        return monthNames[monthIndex] + ' ' + day + ', ' + year;
    }

    /*FILTRE SUR LA BARRE DE RECHERCHE*/
    var path = $('#searchInput').attr('data-path');
    $(document).on('input', 'input#searchInput', function (e) {
        e.preventDefault();
        that = $(this);
        $.ajax({
            url: path,
            type: "POST",
            data: {
                'search': $('input#searchInput').val()
            },
            dataType: 'json',
            success: function (snippet) {
                var tableBody = $('#tableBody');
                var bodyContent = $(tableBody).html();

                var snippetArray = JSON.parse(snippet);
                $('div.table-responsive>p').html('');
                if (snippetArray.length === 0) {
                    $('div.table-responsive').prepend('<p>No snippets found!</p>');
                } else {
                    tableBody.html('');
                    /*recherche des langages associes au snippet*/
                    for (var i = 0; i < snippetArray.length; i++) {
                        var snippetPath = "/snippetDetails/" + snippetArray[i].id;
                        var files = [];
                        var langs = '';
                        var date = new Date(snippetArray[i].date.timestamp * 1000);
                        files = snippetArray[i].files;
                        for (var j = 0; j < files.length; j++) {
                            if (j !== files.length - 1) {
                                langs += files[j].language.name + ', ';
                            } else {
                                langs += files[j].language.name;
                            }
                        }
                        /*mise à jour du tableau*/
                        tableBody.append(
                            '<tr>' +
                            '<td><a href=' + snippetPath + '>' + (i + 1) + '</a></td>' +
                            '<td><a href=' + snippetPath + '>' + snippetArray[i].title + '</a></td>' +
                            '<td>' + langs + '</td>' +
                            '<td>' + snippetArray[i].user.username + '</td>' +
                            '<td>' + formatDate(date) + '</td>' +
                            '<td></td>' +
                            '</tr>'
                        );
                    }
                }
            },
            error: function (a, b, c) {
                console.log(a + b + c);
            }
        });
        return false;
    });
})